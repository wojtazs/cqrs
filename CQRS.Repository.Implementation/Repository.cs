﻿using System;
using System.Collections.Generic;
using System.IO;
using CQRS.Model;
using CQRS.Repository.Interface;

namespace CQRS.Repository.Implementation
{
    public class Repository : IRepository
    {
        const string FileName = @"C:\Users\Wojciech Galant\animals.txt";
        const char Delimiter = '|';

        public void Create(Animal animal)
        {
            var line = Serialize(animal);

            File.AppendAllText(FileName, line);
        }

        public IEnumerable<Animal> GetAnimals()
        {
            var lines = File.ReadAllLines(FileName);

            foreach (var line in lines)
            {
                yield return Deserialize(line);
            }
        }

        private string Serialize(Animal animal) 
            => $"{animal.Name}{Delimiter}{animal.Age}{Environment.NewLine}";

        private Animal Deserialize(string line)
        {
            var items = line.Split(Delimiter);

            return new Animal
            {
                Name = items[0],
                Age = int.Parse(items[1])
            };
        }
    }
}
