using CQRS.ApplicationLogic.CommandBus;
using CQRS.ApplicationLogic.CommandHandlers;
using CQRS.ApplicationLogic.Commands;
using CQRS.ApplicationLogic.Queries;
using CQRS.ApplicationLogic.QueryBus;
using CQRS.ApplicationLogic.QueryHandlers;
using CQRS.Model;
using CQRS.Repository.Interface;
using System;
using System.Collections.Generic;
using Unity;

namespace CQRS
{
    public static class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container =
          new Lazy<IUnityContainer>(() =>
          {
              var container = new UnityContainer();
              RegisterTypes(container);
              return container;
          });

        public static IUnityContainer Container => container.Value;
        #endregion

        public static void RegisterTypes(IUnityContainer container)
        {
            container.RegisterInstance(container);

            container.RegisterType<ICommandBus, CommandBus>();
            container.RegisterType<ICommandHandler<CreateAnimalCommand>, CreateAnimalCommandHandler>();

            container.RegisterType<IQueryBus, QueryBus>();
            container.RegisterType<IQueryHandler<GetAnimalsQuery, IEnumerable<Animal>>, GetAnimalsQueryHandler>();

            container.RegisterType<IRepository, CQRS.Repository.Implementation.Repository>();
        }
    }
}