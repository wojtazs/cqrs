﻿using CQRS.ApplicationLogic.CommandBus;
using CQRS.ApplicationLogic.Commands;
using CQRS.ApplicationLogic.Queries;
using CQRS.ApplicationLogic.QueryBus;
using CQRS.Model;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;

namespace CQRS.Controllers
{
    public class AnimalsController : Controller
    {
        private readonly ICommandBus _commandBus;
        private readonly IQueryBus _queryBus;

        public AnimalsController(ICommandBus commandBus, IQueryBus queryBus)
        {
            _commandBus = commandBus;
            _queryBus = queryBus;
        }

        [HttpPost]
        public ActionResult Create(CreateAnimalCommand command)
        {
            _commandBus.SendCommand(command);

            return new HttpStatusCodeResult(HttpStatusCode.Accepted);
        }

        [HttpGet]
        public ActionResult GetAll(GetAnimalsQuery query)
        {
            var result = _queryBus.Process<GetAnimalsQuery, IEnumerable<Animal>>(query);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}