﻿using CQRS.Model;
using System.Collections.Generic;

namespace CQRS.Repository.Interface
{
    public interface IRepository
    {
        void Create(Animal animal);

        IEnumerable<Animal> GetAnimals();
    }
}
