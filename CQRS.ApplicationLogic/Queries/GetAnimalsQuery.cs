﻿using CQRS.Model;
using System.Collections.Generic;

namespace CQRS.ApplicationLogic.Queries
{
    public class GetAnimalsQuery : IQuery<IEnumerable<Animal>>
    {
    }
}