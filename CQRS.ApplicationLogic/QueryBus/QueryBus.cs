﻿using CQRS.ApplicationLogic.Queries;
using CQRS.ApplicationLogic.QueryHandlers;
using Unity;

namespace CQRS.ApplicationLogic.QueryBus
{
    public class QueryBus : IQueryBus
    {
        private readonly IUnityContainer _container;

        public QueryBus(IUnityContainer container)
        {
            _container = container;
        }

        public TResult Process<TQuery, TResult>(TQuery query) 
            where TQuery : IQuery<TResult>
        {
            var handler = _container.Resolve<IQueryHandler<TQuery, TResult>>();

            return handler.Execute(query);
        }
    }
}
