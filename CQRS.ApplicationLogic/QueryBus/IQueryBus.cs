﻿using CQRS.ApplicationLogic.Queries;

namespace CQRS.ApplicationLogic.QueryBus
{
    public interface IQueryBus
    {
        TResult Process<TQuery, TResult>(TQuery query)
            where TQuery : IQuery<TResult>;
    }
}
