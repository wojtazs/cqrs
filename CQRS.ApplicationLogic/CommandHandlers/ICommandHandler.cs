﻿using CQRS.ApplicationLogic.Commands;

namespace CQRS.ApplicationLogic.CommandHandlers
{
    public interface ICommandHandler<in TCommand>
        where TCommand : ICommand
    {
        void Handle(TCommand command);
    }
}