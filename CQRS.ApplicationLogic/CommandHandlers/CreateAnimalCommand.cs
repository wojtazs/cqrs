﻿using CQRS.ApplicationLogic.Commands;
using CQRS.Repository.Interface;

namespace CQRS.ApplicationLogic.CommandHandlers
{
    public class CreateAnimalCommandHandler : ICommandHandler<CreateAnimalCommand>
    {
        private readonly IRepository repository;

        public CreateAnimalCommandHandler(IRepository repository)
        {
            this.repository = repository;
        }

        public void Handle(CreateAnimalCommand command)
        {
            var animalModel = new CQRS.Model.Animal { Name = command.Name, Age = command.Age };

            repository.Create(animalModel);
        }
    }
}