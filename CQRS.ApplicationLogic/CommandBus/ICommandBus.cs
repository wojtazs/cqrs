﻿using CQRS.ApplicationLogic.Commands;

namespace CQRS.ApplicationLogic.CommandBus
{
    public interface ICommandBus
    {
        void SendCommand<TCommand>(TCommand command)
            where TCommand : ICommand;
    }
}