﻿using CQRS.ApplicationLogic.CommandHandlers;
using CQRS.ApplicationLogic.Commands;
using Unity;

namespace CQRS.ApplicationLogic.CommandBus
{
    public class CommandBus : ICommandBus
    {
        private readonly IUnityContainer _container;

        public CommandBus(IUnityContainer container)
        {
            _container = container;
        }

        public void SendCommand<TCommand>(TCommand command) 
            where TCommand : ICommand
        {
            var handler = _container.Resolve<ICommandHandler<TCommand>>();

            handler.Handle(command); 
        }
    }
}
