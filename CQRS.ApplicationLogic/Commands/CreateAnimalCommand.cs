﻿namespace CQRS.ApplicationLogic.Commands
{
    public class CreateAnimalCommand : ICommand
    {
        public string Name { get; set; }

        public int Age { get; set; }
    }
}