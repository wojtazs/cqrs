﻿using CQRS.ApplicationLogic.Queries;
using CQRS.Model;
using CQRS.Repository.Interface;
using System.Collections.Generic;

namespace CQRS.ApplicationLogic.QueryHandlers
{
    public class GetAnimalsQueryHandler : IQueryHandler<GetAnimalsQuery, IEnumerable<Animal>>
    {
        private readonly IRepository _repository;

        public GetAnimalsQueryHandler(IRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<Animal> Execute(GetAnimalsQuery query)
        {
            return _repository.GetAnimals();
        }
    }
}
