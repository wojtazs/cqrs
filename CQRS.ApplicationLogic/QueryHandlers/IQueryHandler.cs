﻿using CQRS.ApplicationLogic.Queries;

namespace CQRS.ApplicationLogic.QueryHandlers
{
    public interface IQueryHandler<in TQuery, TResult>
        where TQuery : IQuery<TResult>
    {
        TResult Execute(TQuery query);
    }
}